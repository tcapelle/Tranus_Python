#Identify L1E variables after LCAL

from context import pytranus
from pytranus import Lcal, BinaryInterface, L1s, L1sParam, TranusConfig
import numpy as np
import pandas as pd
import logging
import os
from sys import stdout

log_level = logging.DEBUG
logging.basicConfig(format = '%(asctime)s - %(levelname)s - %(message)s',
                    level  = log_level,
                    stream = stdout)
np.set_printoptions(precision=5,linewidth=210,suppress=True)
pd.options.display.width=210

bin = "/home/thomascapelle/TRANUS_binaries/LINUX/"


scn = "00A"
path =  "Grenoble/"
t = TranusConfig(tranusBinPath = bin, workingDirectory = path, 
    			 projectId='NEW', scenarioId=scn) 


interface = BinaryInterface(t)

Lcal = Lcal(t, normalize = False)

nSectors = Lcal.param.nSectors
nZones = Lcal.param.nZones

vec = np.random.random(2*nSectors*nZones)
h0, p0 = Lcal.reshape_vec(vec)

Lcal.calc_sp_housing()

Lcal.param.beta /= 100
Lcal.param.beta[13] = 0.001
p,h, conv, lamda = Lcal.compute_shadow_prices(h0)

##wiriting L1S

L1sparam = L1sParam(t)
path_L1S = os.path.join(path,scn,t.projectId+t.scenarioId+'.L1S')

def read_imploc(imploc_name):
	df = pd.read_csv(os.path.join(path,imploc_name))
	return df
	
def read_L1S(path_L1S, L1sparam):
#241, 25
	out_L1S = L1s(path_L1S,L1sparam.nbSectors,L1sparam.nbTotZones).read() 
	aux = []
	for var in out_L1S:
	    aux.append(var[:,:227])
	return aux
##test 31/8
Lcal.p[0,:] = Lcal.cospro()[0,:]
###
pro, cospro, precio, coscon, utcon, atrac, ajuste, dem = read_L1S(path_L1S, L1sparam)

L1sparam.runParametersExtraction()
L1sparam.GRAL1S(Lcal)   



